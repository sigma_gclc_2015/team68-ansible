#!/usr/bin/env bash
 
# set proxy variables
#export http_proxy=http://myproxy.com:8080
#export https_proxy=https://myproxy.com:8080
 
# install pip
apt-get update
apt-get -y install python-dev python-pip git libxml2-dev libxslt1-dev python-psycopg2 python-jinja2

#cd /home/vagrant
git clone https://github.com/ansible/ansible.git --recursive
cd ./ansible
source ./hacking/env-setup
 
# fix permissions on private/public key file
chmod 600 /home/vagrant/.ssh/id_rsa
chmod 600 /home/vagrant/.ssh/id_rsa.pub
 
# add subject host to known_hosts (IP is defined in Vagrantfile)
ssh-keyscan -H 51.255.62.68 >> /home/vagrant/.ssh/known_hosts
ssh-keyscan -H 79.98.21.68 >> /home/vagrant/.ssh/known_hosts
touch /home/vagrant/.ssh/known_hosts
chown vagrant:vagrant /home/vagrant/.ssh/known_hosts
 
# create ansible hosts (inventory) file
mkdir -p /etc/ansible/
cat /vagrant/hosts > /etc/ansible/hosts
chmod 777 /etc/ansible/hosts