#!/usr/bin/env bash

cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
rm -f /home/vagrant/.ssh/id_rsa.pub
# fix permissions on private key file
chmod 600 /home/vagrant/.ssh/authorized_keys


